import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.List;
import java.util.ArrayList;

public class Servidor {
    private final int PUERTO = 5000;
    private final List<String> datos = new ArrayList<>();

    public String procesar(String entrada) {
      String comando = entrada.split("\s")[0];
      String mensaje = entrada.replaceFirst(".+\s", "");

      switch(comando) {
        case "add":
          datos.add(mensaje);
          return "agregado";
        case "remove":
          datos.remove(mensaje);
          return "eliminado";
        case "show":
          return datos.stream().collect(Collectors.joining("|"));
        default:
          return "opción inválida";
      }
    }

    public void nuevaOrden(Socket sc) {
    	DataInputStream in;
        DataOutputStream out;

        try {
        	System.out.println("Cliente conectado");
            in = new DataInputStream(sc.getInputStream());
            out = new DataOutputStream(sc.getOutputStream());

            //Leo el mensaje que me envia
            String mensaje = in.readUTF();

            String respuesta = procesar(mensaje);

            //Le envio un mensaje
            out.writeUTF(respuesta);

            //Cierro el socket
            sc.close();
            System.out.println("Cliente desconectado");

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public static void main(String[] args) {
    	//Servidor servidor = new Servidor();
    	final Servidor servidor = new Servidor();

        final ServerSocket serverSocket;
        Socket sc = null;


        //puerto de nuestro servidor
        final int PUERTO = 5000;

        try {
            //Creamos el socket del servidor
        	serverSocket = new ServerSocket(PUERTO);
            System.out.println("Servidor iniciado");

            //Siempre estara escuchando peticiones
            while (true) {

                new Thread(() -> {
                	try {
                		servidor.nuevaOrden(serverSocket.accept());
                	} catch (IOException ex) {
                        Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }).start();
            }

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
