import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Cliente {

    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final String HOST = "127.0.0.1";
    private final int PUERTO = 5000;

    public String enviarDatosServidor(String datos) throws IOException {
      Socket sc = new Socket(HOST, PUERTO);
      DataInputStream in = new DataInputStream(sc.getInputStream());
      DataOutputStream out = new DataOutputStream(sc.getOutputStream());

      out.writeUTF(datos);
      String mensajeServidor = in.readUTF();

      sc.close();

      return mensajeServidor;
    }

    public String leerComando() throws IOException {
      System.out.print("> ");
      return reader.readLine();
    }

    public static void main(String[] args) {
        try {
          final Cliente cliente = new Cliente();

          // new Thread(() -> cliente.addDatos(97, 122)).start();
          // new Thread(() -> cliente.addDatos(90, 97)).start();
          // new Thread(() -> cliente.delDatos(97, 120)).start();
          while (true) {
            String datos = cliente.leerComando();
            if (datos.equalsIgnoreCase("q")) {
              break;
            }
            String resultado = cliente.enviarDatosServidor(datos);

            System.out.println(resultado);
          }
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void addDatos(int ini, int fin) {
      for (int i = ini; i<= fin; i++) {
        try {
      		enviarDatosServidor("add " + (char)i);
      	} catch (IOException ex) {
              Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }

    public void delDatos(int ini, int fin) {
      for (int i = ini; i<= fin; i++) {
        try {
      		enviarDatosServidor("remove " + (char)i);
      	} catch (IOException ex) {
              Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
}
